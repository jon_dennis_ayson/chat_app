package com.example.dennis.chat_app;

import android.app.Application;

import com.firebase.client.Firebase;

/**
 * Created by dennis on 4/7/2016.
 */
public class AppLifeCycle  extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Firebase.setAndroidContext(this);
    }
}
