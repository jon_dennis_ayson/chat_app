package com.example.dennis.chat_app;

import java.util.Date;

/**
 * Created by dennis on 4/7/2016.
 */
public class Message {
    private String Text;
    private String Send;
    private Date mDate;

    public Date getmDate(){
        return  mDate;
    }
    public void setmDate(Date mDate) {
        this.mDate = mDate;
    }
    public String getText(){
        return Text;
    }
    public String getSend(){
        return Send;
    }
    public void setText(String Text){
        this.Text = Text;
    }
    public void setSend(String Send){
        this.Send = Send;
    }

}
