package com.example.dennis.chat_app;

import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.example.dennis.chat_app.R.drawable.bubble_right_green;

public class MainActivity extends ActionBarActivity implements View.OnClickListener, MessageDataSource.MessageCallsBack {
    public static final String USER_EXTRA = "USER";
    public static final String TAG = "ChatActivity";
    private ArrayList<Message> mMessages;
    private MessageAdapter mAdapter;
    private String mRecipient;
    private android.widget.ListView ListView;
    private Date mLastMessageDate = new Date();
    private String mConvoId;
    private MessageDataSource.MessagesListener mListener;
    List<Message> Message_List;
    private MessageDataSource datasource;
    public static SharedPreferences noteprefs;
    HashMap<String, String> MapListMessages = new HashMap<String, String>();


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Firebase.setAndroidContext(this);
        mRecipient = "dennis";
        ListView = (ListView) findViewById(R.id.messagesList);
        mMessages = new ArrayList<>();
        mAdapter = new MessageAdapter(mMessages);
        ListView.setAdapter(mAdapter);
        setTitle(mRecipient);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        Button sendMessage = (Button) findViewById(R.id.send);
        sendMessage.setOnClickListener(this);
        String[] ids = {"Main-", "dennis"};
        Arrays.sort(ids);
        mConvoId = ids[0] + ids[1];
        mListener = MessageDataSource.addMessagesListener(mConvoId, this);


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onClick(View v) {
        EditText newMessageView = (EditText) findViewById(R.id.newMessage);
        String newMessage = newMessageView.getText().toString();
        newMessageView.setText("");
        Message msg = new Message();
        msg.setmDate(new Date());
        msg.setText(newMessage);
        msg.setSend("dennis");
        MessageDataSource.saveMessage(msg, mConvoId);

    }

    @Override
    public void onMessageAdded(Message message) {
        mMessages.add(message);
        mAdapter.notifyDataSetChanged();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MessageDataSource.stop(mListener);
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.dennis.chat_app/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.dennis.chat_app/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    private class MessageAdapter extends ArrayAdapter<Message> {
        MessageAdapter(ArrayList<Message> messages) {
            super(MainActivity.this, R.layout.message_item, R.id.message, messages);

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = super.getView(position, convertView, parent);
            Message message = getItem(position);
            TextView nameView = (TextView) convertView.findViewById(R.id.message);
            nameView.setText(message.getText());
            LinearLayout.LayoutParams LayoutParams = (LinearLayout.LayoutParams) nameView.getLayoutParams();


            int sdk = Build.VERSION.SDK_INT;
            if (message.getSend().equals("dennis")) {
                if (sdk >= Build.VERSION_CODES.JELLY_BEAN) {
                    nameView.setBackground(getResources().getDrawable(R.drawable.bubble_right_green));
                    LayoutParams.setMargins(30, 20, 30, 0);

                } else {
                    nameView.setBackgroundDrawable(getResources().getDrawable(R.drawable.bubble_right_green));
                    LayoutParams.setMargins(30, 20, 30, 0);
                }
                LayoutParams.gravity = Gravity.RIGHT;
            } else {
                if (sdk >= Build.VERSION_CODES.JELLY_BEAN) {
                    nameView.setBackground(getResources().getDrawable(R.drawable.bubble_left_gray));
                    LayoutParams.setMargins(30, 20, 30, 0);
                } else {
                    nameView.setBackgroundDrawable(getResources().getDrawable(R.drawable.bubble_left_gray));
                    LayoutParams.setMargins(30, 20, 30, 0);
                }
                LayoutParams.gravity = Gravity.RIGHT;
            }
            nameView.setLayoutParams(LayoutParams);
            return convertView;

        }
    }

}
